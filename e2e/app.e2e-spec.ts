import { AngularCourseAppPage } from './app.po';

describe('angular-course-app App', function() {
  let page: AngularCourseAppPage;

  beforeEach(() => {
    page = new AngularCourseAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
